#ifndef __MANDELBROT_H__
#define __MANDELBROT_H__

#include "global.h"

double complex iter_mandelbrot(double complex, double complex);
void color_mandelbrot(png_byte *, unsigned int);

#endif
