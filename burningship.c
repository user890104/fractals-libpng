#include "mandelbrot.h"
#include "burningship.h"

double complex iter_burningship(double complex z, double complex c) {
    __real__ z = fabs(creal(z));
    __imag__ z = fabs(cimag(z));
    return iter_mandelbrot(z, c);
}

void color_burningship(png_byte *pixel, unsigned int i) {
    pixel[0] = 0xFF;
    pixel[1] = 0x7F;
    pixel[2] = i;
    pixel[3] = 0xFF;
}
