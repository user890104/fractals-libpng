#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include <math.h>
#include <complex.h>

#define PNG_DEBUG 3
#include <png.h>

#endif
