#include "global.h"

typedef double complex (* iter_func_p)(double complex, double complex);
typedef void (* color_func_p)(png_byte *, unsigned int);

struct fractal_def {
    char *name;
    float offset_x;
    float offset_y;
    float ratio_width;
    float ratio_height;
    iter_func_p iter_func;
    color_func_p color_func;
};

struct fractal_def *fractals_get(unsigned int type);
unsigned int fractals_get_iterations(const double complex c, iter_func_p);
void fractals_write(png_bytep *, unsigned int, unsigned int, unsigned int);
