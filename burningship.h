#ifndef __BURNINGSHIP_H__
#define __BURNINGSHIP_H__

#include "global.h"

double complex iter_burningship(double complex, double complex);
void color_burningship(png_byte *, unsigned int);

#endif
