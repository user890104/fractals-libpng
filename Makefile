TARGET=main
SOURCES=main.c fractals.c mandelbrot.c burningship.c
OBJECTS=$(SOURCES:.c=.o)

LIBS=libpng16 zlib
CFLAGS=$(shell pkg-config --cflags $(LIBS)) -g -Wpedantic
LDFLAGS=$(shell pkg-config --libs-only-L $(LIBS))
LOADLIBES=$(shell pkg-config --libs-only-l $(LIBS)) -lm

all: $(SOURCES) $(TARGET)

$(TARGET): $(OBJECTS)

clean:
	rm -rf $(TARGET) $(OBJECTS)
