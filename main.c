#include "global.h"
#include "fractals.h"

void abort_(const char * s, ...) {
    va_list args;
    va_start(args, s);
    vfprintf(stderr, s, args);
    fprintf(stderr, "\n");
    va_end(args);
    abort();
}

unsigned int y, width, height, type;
png_structp png_ptr;
png_infop info_ptr;
png_bytep *row_pointers;

struct fractal_def *fractal;

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "usage: %s <filename> <type> <width>\n", argv[0]);
        return 1;
    }

    type = strtol(argv[2], NULL, 10);
    
    fractal = fractals_get(type);

    if (type > 1) {
        abort_("type > 1");
    }

    width = strtol(argv[3], NULL, 10);

    if (width < 1) {
        abort_("width < 1");
    }

    height = (unsigned int)((float)width * fractal->ratio_height / fractal->ratio_width);

    printf("fractal name: %s\nfractal size: %ux%u\ndest file: %s\n", fractal->name, width, height, argv[1]);
    
    /* create file */
    FILE *fp = fopen(argv[1], "wb");
        
    if (!fp) {
        abort_("[write_png_file] File %s could not be opened for writing", argv[1]);
    }


    /* initialize stuff */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr) {
        abort_("[write_png_file] png_create_write_struct failed");
    }

    info_ptr = png_create_info_struct(png_ptr);

    if (!info_ptr) {
        abort_("[write_png_file] png_create_info_struct failed");
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
        abort_("[write_png_file] Error during init_io");
    }

    png_init_io(png_ptr, fp);


    /* write header */
    if (setjmp(png_jmpbuf(png_ptr))) {
        abort_("[write_png_file] Error during writing header");
    }

    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, 6, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr);

    
    /* allocate memory */
    row_pointers = (png_bytep *) malloc(sizeof(png_bytep) * height);
    
    for (y = 0; y < height; ++y) {
        row_pointers[y] = (png_byte*) malloc(width * 4);
    }


    /* render the fractal */
    fractals_write(row_pointers, width, height, type);


    /* write bytes */
    if (setjmp(png_jmpbuf(png_ptr))) {
        abort_("[write_png_file] Error during writing bytes");
    }

    png_write_image(png_ptr, row_pointers);


    /* end write */
    if (setjmp(png_jmpbuf(png_ptr))) {
        abort_("[write_png_file] Error during end of write");
    }

    png_write_end(png_ptr, NULL);

    /* cleanup heap allocation */

    for (y = 0; y < height; y++) {
        free(row_pointers[y]);
    }

    free(row_pointers);

    fclose(fp);

    return 0;
}
