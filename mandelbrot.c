#include "mandelbrot.h"

double complex iter_mandelbrot(double complex z, double complex c) {
    return z * z + c;
}

void color_mandelbrot(png_byte *pixel, unsigned int i) {
    pixel[2] = i;
    pixel[3] = 0xFF;
}
