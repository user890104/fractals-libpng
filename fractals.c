#include "global.h"
#include "fractals.h"
#include "mandelbrot.h"
#include "burningship.h"

struct fractal_def fractals[] = {
    {
        "mandelbrot",
        -2.5f,
        -1.0f,
        3.5f,
        2.0f,
        iter_mandelbrot,
        color_mandelbrot
    },
    {
        "burningship",
        -2.5f,
        -1.5f,
        3.5f,
        2.0f,
        iter_burningship,
        color_burningship
    }
};

struct fractal_def *fractals_get(unsigned int type) {
    return &fractals[type];
}

unsigned int fractals_get_iterations(const double complex c, iter_func_p iter_func) {
    double complex z = 0 + 0 * I;

    unsigned int i;

    for (i = 0; i < 128; ++i) {
        z = iter_func(z, c);

        if (creall(z) > 2 || cimagl(z) > 2) {
            return i;
        }
    }

    return 0;
}

void fractals_write(png_bytep *rows, unsigned int width, unsigned int height, unsigned int type) {
    png_byte grays[256][4];
    png_byte black[4] = { 0, 0, 0, 0xFF };
    unsigned int i, x, y;
    struct fractal_def *fractal = fractals_get(type);
    double scale = fractal->ratio_width / (float)width;
    
    memset(grays, 0, 256 * 4 * sizeof(png_byte));

    for (i = 0; i < 256; ++i) {
        fractal->color_func((png_byte *)&grays[i], i);
    }

    for (y = 0; y < height; ++y) {
        png_byte* row = rows[y];

        for (x = 0; x < width; ++x) {
            png_byte* pixel = &(row[x * 4]);

            double complex input;
            __real__ input = (double)x * scale + fractal->offset_x;
            __imag__ input = (double)y * scale + fractal->offset_y;

            unsigned int result = fractals_get_iterations(input, fractal->iter_func);
            memcpy(pixel, result ? &grays[result * 2] : &black, 4 * sizeof(png_byte));
        }
    }
}
